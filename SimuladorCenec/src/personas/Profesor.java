/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personas;

import clases.Asignatura;
import clases.CentroEducativo;

/**
 *  Modela un trabajador del centro
 * @author Miguel Páramos
 */
public class Profesor extends Trabajador{
        private int horasSemanales; //Cuánto se lo curra
        private Asignatura[] asignaturasQueImparte; //Las asignaturas que da el profesor
        private CentroEducativo centro; //El centro donde curra
}
