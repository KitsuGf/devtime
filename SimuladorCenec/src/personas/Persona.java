/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personas;

/**
 * Superclase que modela a personas
 * @author Kitsu
 */
public class Persona {
        private String nombre; //Nombre de la Persona
        private String primerApellido; //Primer apellido de la persona
        private String dni; //Su dni
        private String segundoApellido; //Segundo apellido, null si no es hispano
}
