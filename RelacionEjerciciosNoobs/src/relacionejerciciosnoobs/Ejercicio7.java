/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionejerciciosnoobs;

import java.util.Scanner;

/**
 *
 * @author Kitsu
 */
public class Ejercicio7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int contArray;
        contArray = sc.nextInt();
        int[] array = new int[contArray];
        for (int i = 0; i < contArray; i++) {
            array[i] = i;
            System.out.print(array[i] + " ");
        }
        System.out.println("");
        System.out.println("-------------");
        System.out.println("La suma de el array es: " +suma(array));

    }

    public static int suma(int[] array) {

        int suma = 0;
        for (int i = 0; i < array.length; i++) {
            suma += array[i];
        }
        return suma;

    }
}
