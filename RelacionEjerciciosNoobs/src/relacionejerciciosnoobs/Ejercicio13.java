/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionejerciciosnoobs;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Kitsu
 */
public class Ejercicio13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rnd = new Random();
        int contArray;
        contArray = sc.nextInt();
        int[] array = new int[contArray];

        int contPar = 0;
        int contImp = 0;
        for (int i = 0; i < contArray; i++) {
            array[i] = rnd.nextInt(100) + 1;

            if (array[i] % 2 == 0) {
                contPar++;
                //par[i] = array[i];

            } else if (array[i] % 2 != 0) {
                contImp++;
                //impar[i] = array[i];

            }
        }
        int[] impar = new int[contImp];
        int[] par = new int[contPar];
        int tempcontpar = 0;
        int tempcontimpar = 0;
        for (int i = 0; i < contArray; i++) {

            if (array[i] % 2 == 0) {
                par[tempcontpar] = array[i];
                tempcontpar++;
            }
        }
        for (int i = 0; i < contArray; i++) {
            int tempcont = 0;
            if (array[i] % 2 != 0) {
                impar[tempcontimpar] = array[i];
                tempcontimpar++;

            }
        }
        System.out.println("");
        System.out.println(Ejercicio10.imprimeArray(array));
        System.out.println("");
        System.out.println(Ejercicio10.imprimeArray(impar));
        System.out.println("");
        System.out.println(Ejercicio10.imprimeArray(par));
    }

}
