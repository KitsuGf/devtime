/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionejerciciosnoobs;

import java.util.Scanner;

/**
 *
 * @author Kitsu
 */
public class Ejercicio20 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Scanner sc = new Scanner (System.in);
        int n1 = sc.nextInt();
        int n2 = sc.nextInt();
        float [][] matrix = new float [n1][n2];
        int total = n1 * n2;
        System.out.println("--------");
        rellenar(matrix, total);
        
    }
    
    
    public static void rellenar (float[][] matrix, int total){
        String m = "";
        float cont = 0;
        
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] =(float)cont;
                cont++;
            }
            
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                m += matrix[i][j]+1 + "/" + total + " | " + " ";
            }
            
            m += "\n";
        }
        System.out.println(m);
    }
}
